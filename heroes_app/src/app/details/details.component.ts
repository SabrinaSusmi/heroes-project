import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { HeroesService } from '../heroes.service';
import { NgForm } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  // @Input() setHero : any ='';
  // colorCode = '#ffbaba';

  setHero : any = '';
  activeForm = false;

  constructor(private _heroService : HeroesService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.setHero = this._heroService.getHero(this.route.snapshot.paramMap.get('id'));
  }

  setColor(power : any) : string{
    // console.log(power);
    if(power >=80){
      return '#CB4335';
    }else if(power >=60){
      return '#E74C3C';
    }else if(power >=40){
      return '#EC7063';
    }else if(power >=20){
      return '#F1948A'
    }else if(power >=0){
      return '#F5B7B1'
    }else{
      return '#ffffff'
    }
  }

  onSubmit(heroForm : NgForm){
    // console.log(heroForm.value);
    this._heroService.updateHero(this.route.snapshot.paramMap.get('id'),heroForm.value.name,heroForm.value.details);
    this.activeForm = !this.activeForm;
  }

  showForm () {
    this.activeForm = !this.activeForm;
  }

}
