import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  private hero : any;

  private heroes : {id:number, name:string, power:number, details:string, age:number, revelation: Date}[] = [
    { id: 12, name: 'Spiderman', power: 25, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 17, revelation: new Date(2021, 5, 16)},
    { id: 13, name: 'Ms. Marvel', power: 11, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 19, revelation: new Date(2020, 7, 9) },
    { id: 14, name: 'Iron Man', power: 84, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 23, revelation: new Date(2017, 11, 20) },
    { id: 15, name: 'Birdman', power: 9, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 18, revelation: new Date(2019, 8, 5) },
    { id: 16, name: 'Cap America', power: 70, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 31, revelation: new Date(2011, 1, 13) },
    { id: 17, name: 'Thor', power: 67, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 20, revelation: new Date(2018, 9, 25) },
    { id: 18, name: 'Natasha', power: 17, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 15, revelation: new Date(2022, 3, 10) },
    { id: 19, name: 'Hulk', power: 50, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 27, revelation: new Date(2016, 10, 5) },
    { id: 20, name: 'Dr. Strange', power: 32, details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', age: 22, revelation: new Date(2020, 6, 20) }
  ];

  constructor() { }

  getHeroList() {
    return this.heroes;
  }

  getHero(id : any) : any {
    return this.heroes.find(hero => hero.id === +id);
  }

  updateHero(id:any, name:any, description:any) {
    this.hero = this.heroes.find(hero => hero.id === +id);
    let index = this.heroes.indexOf(this.hero);
    this.hero.name = name;
    this.hero.details = description;
    this.heroes[index] = this.hero;
    console.log(this.heroes);

  }
}
