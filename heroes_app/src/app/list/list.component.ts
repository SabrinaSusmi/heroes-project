import { HeroesService } from './../heroes.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Output() selectedHero = new EventEmitter();
  activeHero : any;
  public heroes : any = []

  constructor(private _heroService : HeroesService) {
    // this.heroes = _heroService.getHeroList();
    // this.heroes.sort((a: { power: number; },b: { power: number; }) => a.power-b.power);
   }

  ngOnInit(): void {
    this.heroes = this._heroService.getHeroList();
    this.heroes.sort((a: { power: number; },b: { power: number; }) => a.power-b.power);
  }





  onSelect(hero : any) {
    // console.log(hero);
    this.activeHero = hero;
    this.selectedHero.emit(hero);
  }

}
